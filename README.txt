Proyecto final para Integración de Sistemas en la UAM Cuajimalpa
Realizado por: 
 Alberto Nieto Rocha
 Arantxa Garayzar Cristerna

Se necesita instalar Flask, ElementTree, lxml. 
 
Nosotros instalamos utilizando PIP

Instrucciones:

1. Primero ejecutar el servidor
2. Abrir el navegador y colocar la dirección dada por el servidor (localhost)
3. Utilizar la barra de navegación para navegar por el sitio.