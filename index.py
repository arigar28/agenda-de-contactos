# Proyecto final para integración de sistemas
# Realizado por Arantxa Garayzar Cristerna y Alberto Nieto Rocha

# Agenda de contactos
# Sitio web que permite guardar contactos y almacenarlos en XML

from flask import Flask, render_template, request 
import xml.etree.ElementTree as ET # Biblioteca ParserXM
from lxml import etree
from bs4 import BeautifulSoup


app = Flask(__name__) # Se crea la aplicacion creada con Flask+Python3
tree = ET.parse('XML/agenda.xml')
root = tree.getroot()



""" 
PARTES DEL SITIO
"""
# Pagina de Inicio
@app.route("/")
def inicio():
    return render_template('home.html') # Regresamos el HTML que contiende el sitio

# Pagina para agregar un nuevo contacto
@app.route("/agregar", methods=['GET', 'POST'])
def agregar():
    default_name = '0'
    nombre = request.form.get('fname', default_name)
    apellidos = request.form.get('lname', default_name)
    direccion = request.form.get('address1', default_name)
    estado = request.form.get('state', default_name)
    codigoPostal = request.form.get('zip', default_name)
    ciudad = request.form.get('city', default_name)
    correoElectronico = request.form.get('eaddress', default_name)
    numTel = request.form.get('phone', default_name)
    numCel = request.form.get('celPhone', default_name)

    # Agregar un cliente
    element = ET.Element("contacto") # Se crea un nuevo elemento contacto
    #element.set('nombre', str(nombre))
    ###

    nombreXML = ET.SubElement(element, "nombre") # Agregamos el contenido obtenido del Form a el nuevo elemento del XML
    nombreXML.text = nombre

    apellidosXML = ET.SubElement(element, "apellidos") 
    apellidosXML.text = apellidos

    direccionXML = ET.SubElement(element, "direccion") 
    direccionXML.text = direccion

    estadoXML = ET.SubElement(element, "estado") 
    estadoXML.text = estado

    codigoPostalXML = ET.SubElement(element, "codigoPostal")
    codigoPostalXML.text = codigoPostal

    ciudadXML = ET.SubElement(element, "ciudad")
    ciudadXML.text = ciudad

    correoEXML = ET.SubElement(element, "correoE") 
    correoEXML.text = correoElectronico

    numeroTelXML = ET.SubElement(element, "numeroTelefono") 
    numeroTelXML.text = numTel

    numCelXML = ET.SubElement(element, "numeroCelular") 
    numCelXML.text = numCel

    root.append (element)
    tree.write("XML/agendaCliente.xml")

    if(validateXML("XML/agendaCliente.xml", "XML/agenda.dtd")):
        with open("XML/agendaCliente.xml", 'r') as f:
            with open("/Users/arigar/Desktop/IS/XML/agendaFinal.xml", "w") as f1:
                f1.write("<?xml version='1.0' encoding='ISO-8859-1'?>\n")
                f1.write('<!-- Creado por: Alberto Nieto Rocha Y Arantxa Garayzar Cristerna-->\n')
                f1.write('<?xml-stylesheet type="text/css" href="agendaXML.css" ?>\n')
                f1.write('<!DOCTYPE Agenda SYSTEM "agenda.dtd">\n')
                for line in f:
                    f1.write(line)


    return render_template('agregar.html')

# Pagina para eliminar un contacto
@app.route("/eliminar")
def elimina():
    default_name = '0'

    # Elementos del formulario y variables que almacenan esos datos
    nombreXML = request.form.get('fname', default_name)
    apellidosXML = request.form.get('lname', default_name)
    # Ciclo para buscar dentro del documento XML un contacto por nombre y apellido y borrarlo
    """for contacto in root.findall('contacto'):
        nombreXML = contacto.find('nombre')
        apellidosXML = contacto.find('apellidos')
        if nombre == nombreXML and apellidos == apellidosXML:
            root.remove(contacto)
    tree.write('XML/agenda.xml')"""
    elem = ET.parse("XML/agendaCliente.xml")
    root2 = elem.getroot()
    """name = root2.find("./Agenda/contacto[@nombre='nombreXML']")
    root2.remove(name)
    soup = BeautifulSoup(open('XML/agendaCliente.xml'), 'xml')
    for ext in soup.find_all(('nombre').text):
        removed = ext.extract()
    output = open('XML/agenda23.xml','w')
    output.write(soup.prettify())
    output.close()"""
    """for contacto in root2.findall('nombre'):
        if contacto == 'nombre':
            root2.remove(child)
    tree.write('XML/agenda23.xml')
    
    for contacto in root2.findall('contacto'):
        nombre = str(contacto.find('nombre').text)
        apellidos = str(contacto.find('apellidos').text)
        print(nombre)
        if nombre == nombreXML and apellidos == apellidosXML:
            root2.remove(contacto)

    tree.write('XML/agenda23.xml')
    """ 
    for child in root2.findall("contacto[@nombre='nombreXML']"):
        child.remove(child)
    tree.write('XML/agenda23.xml')
    return render_template('eliminar.html')

# Validar el XML con DTD (Servidor)
def validateXML(filename, dtd):
    xml_file = etree.parse(filename)
    xml_validator = etree.DTD(file=dtd)
    is_valid = xml_validator.validate(xml_file)
    if is_valid:
        return 1
    else: 
        return 0


# Main, ejecutamos la aplicacion web
if __name__ == "__main__":
    app.run(debug=True)